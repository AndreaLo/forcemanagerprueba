pruebaforcemanager
==================

##Andrea López Gañan

He desarrollado la prueba con el framework de Symfony 3.4 y twig. 
Para la comunicación con la API he utilizado el bundle de Guzzle.
http://docs.guzzlephp.org/en/stable/
He implementado un servicio ‘apiservice’ para gestionar todas las peticiones a la api y poder llamarla desde cualquier contexto de la aplicación. En este caso debido a la simplicidad de la arquitectura solo se ejecuta desde el controlador.

Localización de los archivos dentro de el esqueleto del projecto:

###Vistas:

    -pruebaforcemanager/app/Resources/views
    -pruebaforcemanager/app/Resources/views/default

###Controladores:

    -pruebaforcemanager/src/AppBundle/Controller

###Traducciones:

    -pruebaforcemanager/src/AppBundle/Resource/translations

###ApiService:

    -pruebaforcemanager/src/AppBundle/Services

###Configuración:

    -pruebaforcemanager/src/AppBundle/Resource/config
    -pruebaforcemanager/app/config

##Extras
**Eliminar:** he implementado en la tabla un botón de ‘Eliminar’ para cada uno de los registros. La API me retorna un status code 200, sin embargo no se elimina el registro, lo he probado con POSTMAN y aunque retorna un code 200 tampoco se efectúa la eliminación del registro.
He agregado a la url el status code, si presionas sobre el botón prodrás verificarlo con la URL.

**Campos extra en el formulario de modificar y agregar**: He agregado los campos ‘phone2’ y ‘website’.

**Ordenar por algún campo**: En la parte superior de la tabla se encuentra la opción de ordenar por:  Fecha de creación o Nombre. En el caso de fecha de creación, aunque no se aprecie por el formato en el que la fecha se muestra toma en cuenta la hora. Se gestiona desde el controlador .

**Multiidioma** La aplicación por defecto se abre en castellano, en la parte superior derecha se encuentra el selector de idioma. El idioma se gestiona según la variable locale que podemos encontrar en las rutas ‘/es | /en’. Para llevarlo acabo, he utilizado la implementación propia de symfony.

###Requisitos:
Tener composer instalado
https://getcomposer.org/download/
###Pasos para ejecutar la app:

Desde un terminal posicionarnos en la carpeta del projecto y lanzar los siguientes comandos:

    -composer install
    -php bin/console cache:clear
    -php bin/console server:run