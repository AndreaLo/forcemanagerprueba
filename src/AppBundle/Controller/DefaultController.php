<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        $apiservice = $this->get('api.service');
        $accounts = $apiservice->getListAccounts();
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'origen' => 0,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'apitoken' => $apiservice->getApiToken(),
            'accounts' => $accounts
        ]);
    }

    public function indexOrderAction(Request $request)
    {
        $apiservice = $this->get('api.service');
        $accounts = $apiservice->getListAccounts();
        $id = $request->get('order');
        $aux = array();
        if($id == 1) { //Order by data create
            $origen = 1;
            foreach ($accounts as $key =>  $fila){
                $aux[$key] = $fila->dateCreated;
            }
            array_multisort($aux, SORT_ASC, $accounts);
        }else if($id == 2){ // Order by name (2)
            $origen = 2;
            foreach ($accounts as $key =>  $fila){
                $aux[$key] = $fila->name;
            }
            array_multisort($aux, SORT_ASC, $accounts);
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'origen' => $origen,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'apitoken' => $apiservice->getApiToken(),
            'accounts' => $accounts
        ]);
    }

    public function addAccountAction(Request $request){
        $apiservice = $this->get('api.service');
        $accountTypes = $apiservice->getListAccountTypes();
        $listType = array();
        foreach ($accountTypes as $type){
           $listType[$type->descriptionES] = $type->id;
        }
        $defaultData = array();

        $form = $this->createFormBuilder($defaultData)
            ->add('name',TextType::class, array(
                'required' => true
            ))
            ->add('salesRepId1',TextType::class, array(
                'required' => true
            ))
            ->add('accountTypes',ChoiceType::class, array(
                'choices' => $listType,
                'required' => true
            ) )
            ->add('address1',TextType::class, array(
                'required' => false
            ))
            ->add('city',TextType::class, array(
                'required' => false
            ))
            ->add('postcode', TextType::class, array(
                'required' => false
            ))
            ->add('phone',NumberType::class, array(
                'required' => false
            ))
            ->add('phone2', NumberType::class, array(
                'required' => false
            ))
            ->add('email',EmailType::class, array(
                'required' => false
            ))
            ->add('website', TextType::class, array(
                'required' => false
            ))
            ->add('comment', TextareaType::class, array(
                'required' => false
            ))
            ->add('send',SubmitType::class,[
                'attr' =>[
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

            $form->handleRequest($request);

            if ($form-> isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $data['phone'] = ''.$data['phone'];
                $data['phone2'] = ''.$data['phone2'];
                $res = $apiservice->addAccount($data);
                return $this->redirectToRoute('homepage');

            }
        return $this->render('default/addAccount.html.twig', [
            'form' => $form->createView(),
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }


    public function modifyAction($id, Request $request){
        $apiservice = $this->get('api.service');
        $accountTypes = $apiservice->getListAccountTypes();
        $listType = array();
        foreach ($accountTypes as $type){
            $listType[$type->descriptionES] = $type->id;
        }
        $defaultData = array();

        $dataAccounts = $apiservice->getAccountById($id);

        $form = $this->createFormBuilder($defaultData)
            ->add('name',TextType::class, array(
                'required' => true,
                'data' => $dataAccounts->name
            ))
            ->add('salesRepId1',TextType::class, array(
                'required' => true,
                'data' => $dataAccounts->salesRepId1->value
            ))
            ->add('accountTypes',ChoiceType::class, array(
                'choices' => $listType,
                'required' => true,
                'data' => $dataAccounts->typeId->id
            ) )
            ->add('address1',TextType::class, array(
                'required' => false,
                'data' => $dataAccounts->address1
            ))
            ->add('city',TextType::class, array(
                'required' => false,
                'data' => $dataAccounts->city
            ))
            ->add('postcode', TextType::class, array(
                'required' => false,
                'data' => $dataAccounts->postcode
            ))
            ->add('phone',NumberType::class, array(
                'required' => false,
                'data'=> $dataAccounts->phone
            ))
            ->add('phone2',NumberType::class, array(
                'required' => false,
                'data'=> $dataAccounts->phone2
            ))
            ->add('email',EmailType::class, array(
                'required' => false,
                'data' => $dataAccounts->email
            ))
            ->add('website', TextType::class, array(
                'required' => false,
                'data' => $dataAccounts->website
            ))
            ->add('comment', TextareaType::class, array(
                'required' => false,
                'data' => $dataAccounts->comment
            ))
            ->add('send',SubmitType::class,[
                'attr' =>[
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form-> isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data['phone'] = ''.$data['phone'];
            $data['phone2'] = ''.$data['phone2'];
            $res = $apiservice->modifyAccount($id, $data);
            return $this->redirectToRoute('homepage');

        }
        return $this->render('default/modifyAccount.html.twig', [
            'form' => $form->createView(),
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    public function deleteAction($id){
        $apiservice = $this->get('api.service');
        $res = $apiservice->deleteAccount($id);

        return $this->redirectToRoute('homepage', array('msg'=>$res));
    }
}
