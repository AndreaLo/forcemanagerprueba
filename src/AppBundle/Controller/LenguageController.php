<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LenguageController extends Controller
{

    public function englishAction(Request $request)
    {
        $request->setLocale('en');
        return new JsonResponse(true);

    }
    public function castellanoAction(Request $request)
    {
        $request->getSession()->set('_locale','es');
        return $this->redirect($request->headers->get('referer'));
    }

}
