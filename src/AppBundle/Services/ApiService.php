<?php

namespace AppBundle\Services;


use Symfony\Component\HttpFoundation\Session\SessionInterface;
use GuzzleHttp\Client;

class ApiService
{
    private $session;
    private $api_url;
    private $api_token;
    private $client_id;
    private $client_secret;

    public function __construct(SessionInterface $session, $api_url, $client_id, $client_secret)
    {
        $this->session = $session;
        $this->api_url = $api_url;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->getToken();
    }

    private function getToken(){
        $client = new Client();
        $authparams = array(
            'username' => $this->client_id,
            'password' => $this->client_secret,
        );
        $params = array(
            'body' => json_encode($authparams),
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ]
        );

        $response = $client->post($this->getApiUrl().'/login',$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);
        $this->setApiToken($jsonresponse->token);
    }

    public function getListAccounts(){
        $client = new Client();
        $params = array(
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
                'X-Session-Key' => $this->getApiToken()
            ]
        );

        $response = $client->get($this->getApiUrl().'/accounts',$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);
        return $jsonresponse;
    }

    public function getListAccountTypes(){
        $client = new Client();
        $params = array(
        'headers' => [
            'Accept' => '*/*',
            'Content-Type' => 'application/json',
            'X-Session-Key' => $this->getApiToken()
        ]
        );

        $response = $client->get($this->getApiUrl().'/accountTypes',$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);
        return $jsonresponse;
    }

    public function addAccount($data){
        $client = new Client();
        $authparams = array(
            "address1" => $this->scapeComma($data['address1']),
            "city"=> $this->scapeComma($data['city']),
            "comment"=> $data['comment'],
            "email"=> $data['email'],
            "name"=> $data['name'],
            "phone"=> $data['phone'],
            "postcode"=> $data['postcode'],
            "salesRepId1"=> 103,
            "typeId"=> $data['accountTypes'],
        );
        $params = array(
            'body' => json_encode($authparams),
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
                'X-Session-Key' => $this->getApiToken()
            ]
        );

        $response = $client->post($this->getApiUrl().'/accounts',$params);
        if($response->getStatusCode()== 200){
            return true;
        }else{
            return false;
        }
    }

    public function deleteAccount($id){
        $client = new Client();
        $params = array(
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
                'X-Session-Key' => $this->getApiToken()
            ]
        );

        $response = $client->delete($this->getApiUrl().'/accounts/'.$id,$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);

        return $response->getStatusCode();
    }

    public function modifyAccount($id, $data){
        $client = new Client();
        $authparams = array(
            "address1" => $this->scapeComma($data['address1']),
            "city"=> $this->scapeComma($data['city']),
            "comment"=> $data['comment'],
            "email"=> $data['email'],
            "name"=> $data['name'],
            "phone"=> $data['phone'],
            "postcode"=> $data['postcode'],
            "salesRepId1"=> 103,
            "typeId"=> $data['accountTypes'],
        );
        $params = array(
            'body' => json_encode($authparams),
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json; charset=utf-8',
                'X-Session-Key' => $this->getApiToken()
            ]
        );

        $response = $client->put($this->getApiUrl().'/accounts/'.$id,$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);
        return $jsonresponse;
    }

    public function getAccountById($id){
        $client = new Client();
        $params = array(
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json',
                'X-Session-Key' => $this->getApiToken()
            ]
        );

        $response = $client->get($this->getApiUrl().'/accounts/'.$id,$params);
        $body = $response->getBody();
        $jsonresponse = json_decode($body);
        return $jsonresponse;
    }

    /**
     * @return SessionInterface
     */
    private function getSession()
    {
        return $this->session;
    }

    /**
     * @return mixed
     */
    private function getApiUrl()
    {
        return $this->api_url;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->api_token;
    }

    /**
     * @return mixed
     */
    private function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @return mixed
     */
    private function getClientSecret()
    {
        return $this->client_secret;
    }

    /**
     * @param mixed $api_token
     */
    private function setApiToken($api_token)
    {
        $this->api_token = $api_token;
    }

    private function scapeComma($string){
        return str_replace("''","\'",$string);
    }
}